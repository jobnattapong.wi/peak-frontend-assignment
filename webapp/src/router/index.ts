import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Products from "../views/Products.vue";
import CartItem from "../views/CartItem.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "products",
    component: Products,
  },
  {
    path: "/cart",
    name: "carts",
    component: CartItem,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;

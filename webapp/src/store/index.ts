import Vue from "vue";
import Vuex from "vuex";
import axios from 'axios'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    products : [],
    cartItems: []
  },
  getters: {
    getProducts: (state) => state.products,
    cartItemCount: (state) => state.cartItems.length,
    getCartItem: (state) => state.cartItems,
  },
  mutations: {
    setProducts(state: any, data: any) {
      state.products = data
    },
    addToCart(state: any, product: any) {
      state.cartItems.push(product);
    },
    deleteFromCart(state, productId) {
      state.cartItems = state.cartItems.filter((item: any) => item.id !== productId);
    }
  },
  actions: {
    async fetchProducts({ commit }) {
      await axios.get('http://localhost:3000').then(res => {
        commit('setProducts', res.data.products)
      })
    },
    addProductToCart({ commit }, product) {
      commit('addToCart', product);
    },
    deleteProductFromCart({ commit }, productId) {
      commit('deleteFromCart', productId);
    }
  },
  modules: {},
});
